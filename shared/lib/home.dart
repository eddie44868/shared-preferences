import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController name = new TextEditingController();
  TextEditingController username = new TextEditingController();
  TextEditingController _dateTime = new TextEditingController();
  String nama = "", Username = "", Date = "";
  DateTime date = DateTime(2023, 03, 21);

  Future<Null> simpan() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("name", name.text);
    prefs.setString("username", username.text);
    prefs.setString("Date", _dateTime.text);
  }

  Future<Null> show() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    nama = prefs.getString("name")!;
    Username = prefs.getString("username")!;
    Date = prefs.getString("Date")!;
  }

  @override
  void initState() {
    super.initState();
    simpan();
    show();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: 
        Scaffold(
          resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text("Shared Preferences"),
          backgroundColor: Colors.purple,
        ),
        body: Padding(
          padding: EdgeInsets.only(left: 20, right: 10, top: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Name", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),),
                  SizedBox(height: 10),
                  Container(
                    height: 35,
                    width: 300,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey[200],
                    ),
                    child: TextFormField(
                      controller: name,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 5.0, 5.0),
                        filled: true,
                        fillColor: Colors.grey[100]
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 30),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Username", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),),
                  SizedBox(height: 10),
                  Container(
                    height: 35,
                    width: 300,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey[200],
                    ),
                    child: TextFormField(
                      controller: username,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 5.0, 5.0),
                        filled: true,
                        fillColor: Colors.grey[100]
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 30),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Birth Date", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),),
                  SizedBox(height: 10),
                  InkWell(
                    onTap: () async {
                      DateTime? newDate = await showDatePicker(
                        builder: (context, child) {
                          return Theme(
                            data: ThemeData().copyWith(
                              colorScheme: ColorScheme.light(
                                primary: Colors.purple
                              )
                            ), child: child!, 
                        )
                        },
                        context: context, 
                        initialDate: date, 
                        firstDate: DateTime(1950), 
                        lastDate: DateTime(2100));
                        if (newDate == null) return;
                        setState(() {
                          date = newDate;
                        });
                    },
                    child: Container(
                      height: 35,
                      width: 200,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                          padding:EdgeInsets.only(left: 10),
                          child: Row(
                            children: [
                              Image.asset("assets/calendar.png", height: 20, width: 20),
                              SizedBox(width: 10),
                              (date == DateTime(2023, 03, 21)) ? 
                              Text("Pick Date", style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                                color: Colors.grey
                              )) : Text("${date.day}/${date.month}/${date.year}", style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                                color: Colors.grey
                              ))
                            ],
                          ),
                        ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: FloatingActionButton.extended(
                      backgroundColor: Colors.purple,
                      onPressed: () async {
                        _dateTime.text = "${date.day}/${date.month}/${date.year}";
                        await simpan();
                        name.text = '';
                        username.text = '';
                        _dateTime.text = '';
                        date = DateTime(2023, 03, 21);
                      },
                      label: Text('Save', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),),
                      icon: Icon(Icons.edit),
                    ),
                  ),
                  SizedBox(width: 30),
                  Center(
                    child: FloatingActionButton.extended(
                      backgroundColor: Colors.purple,
                      onPressed: () {
                        setState(() {
                          show();
                        });
                      },
                      label: Text('Show', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),),
                      icon: Icon(Icons.edit),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30),
              // ignore: unrelated_type_equality_checks
              (nama == '') ? Container() : Text("Name : $nama", style: TextStyle(color: Colors.black, fontSize: 20)),
              (Username == '') ? Container() : Text("Username : $Username", style: TextStyle(color: Colors.black, fontSize: 20)),
              (Date == '') ? Container() : Text("Birth Date : $Date", style: TextStyle(color: Colors.black, fontSize: 20)),
            ],
          )
        )
     )
    );
  }
}
